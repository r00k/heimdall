import base64
from jinja2 import Environment, FileSystemLoader
import os


class EC2Terraform:
    # Template to be used for generating Terraform compatible output
    TF_TEMPLATE = "ec2_template.jinja"

    instance_name = None
    ami = None
    userdata = None
    instance_type = None

    def __init__(self, instance_name="", tag_name="", ami="", userdata="", instance_type="", out_bucket="", terminate=False):
        self.terminate = terminate
        self.instance_name = instance_name
        self.tag_name = tag_name
        self.ami = ami
        self.userdata = userdata
        self.instance_type = instance_type
        self.out_bucket = out_bucket

    def gen_terraform(self):
        """
        Generate a .tf compatible string for EC2

        :return: str
        """

        # Process OUTPUT_BUCKET variable
        _userdata = self.userdata.replace("OUTPUT_BUCKET", self.out_bucket)

        if self.terminate:
            shutdown_behavior = "terminate"
        else:
            shutdown_behavior = "stop"

        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')))
        tmpl = env.get_template(self.TF_TEMPLATE)
        return "\n" + tmpl.render(instance_name=self.instance_name.replace(" ", ""),
                                  ami=self.ami,
                                  tag_name=self.tag_name,
                                  instance_type=self.instance_type,
                                  shutdown_behavior = shutdown_behavior,
                                  userdata=base64.b64encode(_userdata.encode()).decode("utf-8"))


if __name__ == "__main__":
    example1 = EC2Terraform()
    example1.instance_type = "t2.micro"
    example1.instance_name = "example1"
    example1.ami = "ami-04b9e92b5572fa0d1"
    example1.userdata = "#!/bin/bash\nshutdown"

    print("Example 1\n-------------\n")
    print(example1.gen_terraform())

    example2 = EC2Terraform(instance_name="Foobar",
                            ami="ami-04b9e92b5572fa0d1",
                            userdata="#!/bin/bash\nshutdown",
                            instance_type="t2.micro")

    print("\nExample 2\n-------------\n")
    print(example2.gen_terraform())
