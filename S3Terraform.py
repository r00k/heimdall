from jinja2 import Environment, FileSystemLoader
import os


class S3Terraform:
    # Template to be used for generating Terraform compatible output
    TF_TEMPLATE = "s3_template.jinja"

    bucket = ""

    def __init__(self, bucket=None):
        self.bucket = bucket

    def gen_terraform(self):
        """
        Generate a .tf compatible string for S3ß

        :return: str
        """
        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')))
        tmpl = env.get_template(self.TF_TEMPLATE)
        return "\n" + tmpl.render(bucket_name=self.bucket)


if __name__ == "__main__":
    example1 = S3Terraform()
    example1.bucket = "heimdall-testing"

    print(example1.gen_terraform())
