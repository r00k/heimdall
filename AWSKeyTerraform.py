from jinja2 import Environment, FileSystemLoader
import os

class AWSKeyTerraform:
    TF_TEMPLATE = "aws_key_template.jinja"

    name = None
    pub_key = None

    def __init__(self, name="", pub_key=""):
        self.name = name
        self.pub_key = pub_key

    def gen_terraform(self):
        """
        Generate a .tf compatible string for aws key
        
        :return: str
        """
        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')))
        tmpl = env.get_template(self.TF_TEMPLATE)
        return "\n" + tmpl.render(name=self.name.replace(" ", ""),
                                  pub_key=self.pub_key)
