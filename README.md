# heimdall

#### Install 

Requires Terraform binary to be installed and in your path.

pip3 install -r requirements.txt

#### Usage

Can result in cloud service charges.   

Tool to distribute scanning and recon activities across multiple parallel cloud services.  

Currently only works with AWS.

    usage: heimdall [-h] [-c CONFIG] [-i INSTANCES] [-k KEY]
                    [--terminate-on-shutdown] [--test]
    
    Tool to automate distributed scanning activities across parallel cloud
    instances.
    
    optional arguments:
      -h, --help            show this help message and exit
      -c CONFIG, --config CONFIG
                            Specify YAML input config file
      -i INSTANCES, --instances INSTANCES
                            Number of instances to be spun up
      -k KEY, --key KEY     Specify public key to use for instances
      --terminate-on-shutdown
                            Sets instance shutdown behavior to "terminate"
      --test                Runs in test mode. Generates .tf file but does not run
                            "apply"

sample.yaml will "work" when ran with --test. Descriptions of fields below.

    config:
      campaign_name: "Used to create cloud variable names and file names"
      cred_file: "/path/to/aws/cred/file"
      cred_profile: "[aws profile name]"
      ami: "ami to use"
      region: "region to deploy in"
      instance_type: "instance type to use"
      instance_num: integer for max number of instances to chunk work across
      provider: "cloud provider (aws only valid option right now)"
    tool_prologue:
      - echo "Commands (1 per line) to be ran on each instance before commands are run. Bootstrap command pre-reqs"
      - apt update
      - apt install nmap
    commands: 
      - echo "command to run on machines. commands will be chunked across ~instance_num instances"
      - nmap -p- 10.10.10.0/24 -oA /tmp/output/10.10.10.0
    tool_epilogue:
      - echo "Commands to run after command variable is ran. Primarily for collecting tool output."
      - aws s3 sync /tmp/output s3://OUTPUT_BUCKET/
