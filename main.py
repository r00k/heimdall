#!/usr/bin/env python3

import argparse
import yaml
import time
import os
import random
import string

import EC2Terraform
import AWSTerraform
import S3Terraform
import AWSKeyTerraform

from jinja2 import Environment, FileSystemLoader

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

from collections import namedtuple


def chunk_it(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def ssh_key_gen():
    """
    Generates public/private key pair and returns both in utf-8 format

    :return: [str, str]
    """
    ssh_key = rsa.generate_private_key(backend=default_backend(), public_exponent=65537, \
                                   key_size=2048)
    public_key = ssh_key.public_key().public_bytes(serialization.Encoding.OpenSSH, \
                                               serialization.PublicFormat.OpenSSH)
    pem = ssh_key.private_bytes(encoding=serialization.Encoding.PEM,
                            format=serialization.PrivateFormat.TraditionalOpenSSL,
                            encryption_algorithm=serialization.NoEncryption())
    key_pair = namedtuple("key", ["private", "public"])
    return key_pair(pem.decode('utf-8'), public_key.decode('utf-8'))


def main():
    parser = argparse.ArgumentParser(description="Tool to automate distributed scanning activities across parallel "
                                                 "cloud instances.")
    parser.add_argument("-c", "--config", help="Specify YAML input config file")
    parser.add_argument("-i", "--instances", help="Number of instances to be spun up")
    parser.add_argument("-k", "--key", help="Specify public key to use for instances")
    parser.add_argument("--persistent", default=False, action='store_true', help="Prevent instances from auto shutdown on completion")
    parser.add_argument("--terminate-on-shutdown", default=False, action='store_true', help="Sets instance shutdown behavior to "
                                                                             "\"terminate\"")
    parser.add_argument("--test", action='store_true', help="Runs in test mode. Generates .tf file but does not run \"apply\"")
    args = parser.parse_args()

    terraform_objects = []

    if args.config:
        with open(args.config) as file:
            yaml_dict = yaml.load(file, Loader=yaml.FullLoader)
        commands_length = len(yaml_dict['commands'])
        campaign_name = yaml_dict['config']['campaign_name'].replace(" ", "_")

        # check for existing campaign in current directory
        if os.path.exists(f'{campaign_name}.tf'):
            user_cont = input(f"File {campaign_name}.tf already exists. Continuing will result in loss of that file.\n"
                              "Do you want to continue? Y/n [n]: ")
            if user_cont.lower() != 'y':
                exit("{campaign_name}.tf file exists. Exiting")

        # assign or generate public key for TF instances
        if args.key:
            if os.path.exists(args.key):
                with open(args.key) as key_file:
                    pub_key = key_file.read()
            else:
                exit("Supplied key file does not exist")
        else:
            key_pair = ssh_key_gen()
            with open(f"{campaign_name}.priv", "w") as priv_key:
                priv_key.write(key_pair.private)
            pub_key = key_pair.public

        aws_key = AWSKeyTerraform.AWSKeyTerraform()
        aws_key.pub_key = pub_key.rstrip()
        aws_key.name = campaign_name
        terraform_objects.append(aws_key)

        # generate instance tf documents for ec2 instances
        if yaml_dict['config']['instance_num'] > commands_length:
            chunk_size = 1
        else:
            chunk_size = int(commands_length / yaml_dict['config']['instance_num'])
        chunks = list(chunk_it(yaml_dict['commands'], chunk_size))

        if 'terminate_on_shutdown' in yaml_dict['config']:
            terminate = yaml_dict['config']['terminate_on_shutdown'],
        else:
            terminate = args.terminate_on_shutdown,

        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')))
        if args.persistent == True:
            tmpl = env.get_template("userdata_template_no_shutdown.jinja")
        else:
            tmpl = env.get_template("userdata_template.jinja")
        bucket_name = f"{campaign_name}-{int(time.time())}"
        terraform_objects.append(S3Terraform.S3Terraform(bucket=bucket_name))

        provider = AWSTerraform.AWSTerraform()
        provider.cred_file = yaml_dict['config']['cred_file']
        provider.region = yaml_dict['config']['region']
        provider.profile = yaml_dict['config']['cred_profile']
        terraform_objects.append(provider)

        for i in chunks:
            new_entry = EC2Terraform.EC2Terraform(
                tag_name=campaign_name,
                instance_name=''.join(random.choices(string.ascii_lowercase, k=20)),
                instance_type=yaml_dict['config']['instance_type'],
                ami=yaml_dict['config']['ami'],
                terminate=terminate,
                out_bucket=bucket_name,
                userdata=tmpl.render(
                    tool_prologue="\n".join(yaml_dict['tool_prologue']),
                    commands="; ".join(i),
                    tool_epilogue="\n".join(yaml_dict['tool_epilogue'])
                )
            )
            terraform_objects.append(new_entry)

        with open(f"{campaign_name}.tf", "w") as outfile:
            for tfobj in terraform_objects:
                outfile.write(tfobj.gen_terraform())

        if args.test:
            os.system("terraform validate")
        else:
            os.system("terraform apply -auto-approve")

    else:
        pass


if __name__ == '__main__':
    main()
