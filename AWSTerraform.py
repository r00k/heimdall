import base64
from jinja2 import Environment, FileSystemLoader
import os


class AWSTerraform:
    # Template to be used for generating Terraform compatible output
    TF_TEMPLATE = "aws_template.jinja"

    region = None
    cred_file = None
    profile = None

    def __init__(self, region=None, cred_file=None, profile=None):
        self.region = region
        self.cred_file = cred_file
        self.profile = profile

    def gen_terraform(self):
        """
        Generate a .tf compatible string for EC2

        :return: str
        """
        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')))
        tmpl = env.get_template(self.TF_TEMPLATE)
        return "\n" + tmpl.render(region=self.region,
                                  cred_file=self.cred_file,
                                  profile=self.profile)


if __name__ == "__main__":
    example = AWSTerraform(region="us-east-1",
                           cred_file="/Users/reznok/.aws/credentials",
                           profile="heimdall")

    print(example.gen_terraform())
